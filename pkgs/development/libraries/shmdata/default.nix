{ stdenv
, fetchgit
, lib
, cmake
, gst_all_1
, pcre
, libunwind
, pkgconfig
, bison
, flex
, libtool
, python3
}:

let
    version = "1.3.18";
    debug = false;
in stdenv.mkDerivation {
    name = "shmdata-${version}";

    src = fetchgit {
        rev = "${version}";
        url = "https://gitlab.com/sat-metalab/shmdata.git";
        sha256 = "0c4i344lr0y3sb5wrm102k14fq50dmba93bmwzflq1a5bwwdg32j";
        fetchSubmodules = false;
    };

    # bind gstreamer-1.0 from nix gstreamer packages
    NIX_CFLAGS_COMPILE =
        let gstPluginPaths =
            lib.makeSearchPathOutput "lib" "/lib/gstreamer-1.0"
            (with gst_all_1; [
                gstreamer
                gst-plugins-base
            ]);
        in [
            "-I${gst_all_1.gstreamer.dev}/lib/gstreamer-1.0/include"
            ''-DGST_PLUGIN_PATH_1_0="${gstPluginPaths}"''
        ];

    patches = [
      ./macos-changes.patch
    ];

    buildInputs =  with gst_all_1; [
        gstreamer
        gst-plugins-base
    ] ++ [
        pcre
        libunwind
        pkgconfig
        bison
        flex
        libtool
        gstreamer
        gst-plugins-base
        python3
    ];

    nativeBuildInputs = [ cmake ];

    cmakeFlags = [
        "-DCMAKE_BUILD_TYPE=${if debug then "Debug" else "Release"}"
    ];

    meta = with stdenv.lib; {
        description = "switcher is an integration environment, able to interoperate with other software and protocols";
        homepage = https://gitlab.com/sat-metalab/shmdata;
        license = licenses.gpl2;
        platforms = platforms.unix;
        maintainers = with maintainers; [ metalab ];
    };
}
