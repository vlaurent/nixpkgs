{ stdenv
, fetchgit
, fetchurl
, lib
, cmake
, gst_all_1
, xorg
, shmdata
, pcre
, libunwind
, orc
, pkgconfig
, bison
, flex
, libtool
, glib
, json-glib
, gsoap
, liblo
, libpulseaudio
, portmidi
, libjack2
, jack2
, libvncserver
, libuuid
, openssl
, swh_lv2
, mesa_noglu
, libGL_driver
, freeglut
, libltc
, curl
, fomp
, libsamplerate
, libresample
, python3
, git
, glfw
}:

let
  version = "1.0.0";
  debug = true;

  pjsipPlugin = fetchurl {
    name = "pjsip";
    url = "http://www.pjsip.org/release/2.7/pjproject-2.7.tar.bz2";
    sha256 = "0swgn821nc5m4fcrwbxv2b3iy0zgr2mhc7siqarm34f324d6500w";
  };

  glfwinPlugin = fetchurl {
    name = "glfwin";
    url = "https://github.com/glfw/glfw/archive/3.2.1.tar.gz";
    sha256 = "1s4gklpkqw9hh5cgpyrs9xn0s4gn8ccfjlqc47yfcxad73hhs3z1";
  };

  imguiPlugin = fetchgit {
    name = "imgui";
    url = "https://github.com/ocornut/imgui.git";
    rev = "refs/tags/v1.49";
    sha256 = "03y0j7c2fq7wp9vx9qc01lp6pn21yjckkqf158v5dm5cs8av9ir9";
  };

  syphonPlugin = fetchgit {
    name = "syphon";
    url = "https://github.com/Syphon/Syphon-Framework.git";
    sha256 = "1di6a76mxrn97dlihb2dl2sbx0a7633klqx4wv8ahp40ix25y9cc";
  };

  vrpnPlugin = fetchgit {
    name = "vrpn";
    url = "https://github.com/vrpn/vrpn.git";
    sha256 = "1agsc65yxk7j48k6d1cam57nmmn0ymaz15n3glpdcp2ylfvq16ap";
  };

in stdenv.mkDerivation rec {
  name = "switcher-${version}";

  src = fetchgit {
    rev = "${version}";
    url = "https://gitlab.com/sat-metalab/switcher.git";
    sha256 = "0xqygzhv6mikbcvgf2hjfzrqa5fa3lkky9i7plncwlib8mp26141";
    fetchSubmodules = true;
  };

  # bind gstreamer-1.0 from nix gstreamer packages
  NIX_CFLAGS_COMPILE = let gstPluginPaths =
    lib.makeSearchPathOutput "lib" "/lib/gstreamer-1.0"
    (with gst_all_1; [
      gstreamer
      gst-plugins-base
      gst-plugins-good
      gst-plugins-ugly
      gst-plugins-bad
      gst-libav
    ]); in [
      "-I${gst_all_1.gstreamer.dev}/lib/gstreamer-1.0/include"
      ''-DGST_PLUGIN_PATH_1_0="${gstPluginPaths}"''
    ];

  patches = [
    ./add-external-plugins-changes.patch
  ];

  preBuild = ''
    for sh in /build/switcher/build/plugins/pjsip/*.sh; do
      patchShebangs $sh
    done

    mkdir -p $out/libsrtp-switcher/srtp

    ln -s ${pjsipPlugin} /tmp/pjproject-2.7.tar.bz2
    ln -s ${glfwinPlugin} /tmp/glfwin.tar.gz

    tar -cf /tmp/imgui.tar ${imguiPlugin}
    tar -cf /tmp/syphon.tar ${syphonPlugin}
    tar -cf /tmp/vrpn.tar ${vrpnPlugin}
  '';

  buildInputs =  with gst_all_1; [
    gstreamer
    gst-plugins-base
    gst-plugins-good
    gst-plugins-ugly
    gst-plugins-bad
    gst-libav
  ] ++ (with xorg; [
    libXrandr
    libXinerama
    libXcursor
  ]) ++ [
    shmdata
    pcre
    libunwind
    orc
    bison
    flex
    libtool
    glib
    json-glib
    gsoap
    liblo
    libpulseaudio
    portmidi
    libjack2
    jack2
    libvncserver
    libuuid
    openssl
    swh_lv2
    mesa_noglu
    libGL_driver
    freeglut
    libltc
    curl
    fomp
    libsamplerate
    libresample
    python3
    git
    glfw
  ];

  nativeBuildInputs = [ pkgconfig cmake ];

  # "-DENABLE_GPL=ON"
  cmakeFlags = [
    "-DCMAKE_BUILD_TYPE=${if debug then "Debug" else "Release"}"
  ];

  meta = with stdenv.lib; {
    description = "Library to share streams of framed data between processes via shared memory";
    homepage = https://gitlab.com/sat-metalab/switcher;
    license = licenses.gpl2;
    platforms = platforms.unix;
    maintainers = with maintainers; [ metalab ];
  };
}
